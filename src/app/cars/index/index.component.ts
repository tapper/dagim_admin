import {Component, OnInit} from '@angular/core';
import {CarService} from "../car.service";
import {SettingsService} from "../../../settings/settings.service";
import {MESSAGES} from "../../email/mock-messages";
import { Message } from '../../email/message';






@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', '../../email/email.component.scss', './index.component.css'],
    providers: [CarService]
})


export class IndexComponent implements OnInit {
    
    ItemsArray: any[] = [];
    host: string = '';
    settings = '';
    
    messages: Message[];
    selectedItem;
    messageOpen = false;
    isOpened = true;
    _autoCollapseWidth = 991;
    
    
    constructor(public CarService: CarService, settings: SettingsService) {
        this.CarService.GetAllCars('GetAllCars').then((data: any) => {
            console.log("getAllCars : ", data) ,
                this.ItemsArray = data,
                this.selectedItem = this.ItemsArray[0];
                this.host = settings.host,
                console.log(this.ItemsArray[0].logo)
        })
    }
    
    DeleteItem(i) {
        console.log("Del 1 : ", this.ItemsArray[i].id);
        /*this.CarService.DeleteCompany('DeleteCompany', this.ItemsArray[i].id).then((data: any) => {
            this.ItemsArray = data , console.log("Del 2 : ", data);
        })*/
    }
    
    ngOnInit(): void {
        if (this.isOver()) {
            this.isOpened = false;
        }
        
    }
    
    ngAfterContentInit() {
    
    }
    
    toogleSidebar(): void {
        this.isOpened = !this.isOpened;
    }
    
    isOver(): boolean {
        return window.matchMedia(`(max-width: 991px)`).matches;
    }
    
    
    onSelect(message: Message): void {
        this.selectedItem = message;
        if (this.isOver()) {
            this.isOpened = false;
        }
    }
}
