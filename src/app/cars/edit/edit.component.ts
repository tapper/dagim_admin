import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {CarService} from "../car.service";


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  {

    public Id;
    public navigateTo:string = '/company/index';
    public imageSrc: string = '';
    public Company:any[]=[];

    constructor(private route: ActivatedRoute,private http: Http, public service:CarService , public router:Router) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];
            this.Company = this.service.Companies[this.Id];
            console.log("CP : " ,this.service.Companies)
        });
    }

    onSubmit(form:NgForm)
    {
        console.log("Edit : " , this.Company);
        this.service.EditCompany('EditCompany',this.Company).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : " , reader)
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : " , this.imageSrc)
    }

   //this.http.post('http://tapper.co.il/salecar/laravel/public/api/GetFile', formData, options)

}
