import {Component, OnInit, ElementRef, ChangeDetectionStrategy , ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";

import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

    public navigateTo:string = '/category/index';
    public paramsSub;
    public id: number;
    public SubCategories;
    public isReady;
    public imageSrc: string = '';
    public folderName:string = 'products';
    public rowsNames: any[] = ['שם בעיברית','שם באנגלית','מחיר נמוך','מחיר גבוה'];
    public rows: any[] = ['title','title_english','low_price','high_price'];

    public Item =
        {
            'title':'',
            'title_english':'',
            'low_price':'',
            'high_price':'',
            'description':'',
            'description_english	':'',
            'image':'',
        }

    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router) {
        console.log("Row : " , this.rows)
        this.service.GetItems('GetSubCategoriesById',-1).then((data: any) => {
            this.SubCategories = data;
            this.isReady = true;
            console.log("Sub : " , data);
        });
    }

    onSubmit(form:NgForm)
    {
        console.log(form.value);
        let fi = this.fileInput.nativeElement;
        let fileToUpload;
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}

        this.service.AddItem('AddProduct',form.value,fileToUpload).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }
    
    ngOnInit() {
        this.paramsSub = this.route.params.subscribe(params => this.id = params['id']);
    }
    
    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }

}
