import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {KitchenService} from "../kitchen.service";


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  {

    public Id;
    public navigateTo:string = '/kitchen/index';
    public imageSrc: string = '';
    public Item:any[]=[];

    constructor(private route: ActivatedRoute,private http: Http, public service:KitchenService , public router:Router) {
        this.route.params.subscribe(params => {
            console.log("Edit : " , params['id'] + " : " , this.service.Items[this.Id])
            this.Id = params['id'];
            this.Item = this.service.Items[this.Id];
        });
    }

    onSubmit(form:NgForm)
    {
        console.log("Edit : " , this.Item);
        this.service.EditItem('EditItem',this.Item).then((data: any) => {
            console.log("EDDITCompany : " , data);
           // this.router.navigate([this.navigateTo]);
        });
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : " , reader)
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : " , this.imageSrc)
    }

}
