import {Component, OnInit} from '@angular/core';
import {EmployeeService} from "../employee.service";
import {SettingsService} from "../../../settings/settings.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    Id:number;
   
    
    
    constructor(public EmployeeService:EmployeeService,public settings:SettingsService , private route: ActivatedRoute) {
          this.route.params.subscribe(params => {
              this.Id = params['id'];
              console.log("ssss : " , this.Id )
              if(!this.Id )
                  this.Id = -1;
              
              this.EmployeeService.GetItems('GetEmployees', this.Id).then((data: any) => {
                  console.log("Orders : ", data) ,
                      this.ItemsArray = data,
                      this.ItemsArray1 = data,
                      this.host = settings.host
                      //console.log(this.ItemsArray[0].logo)
              })
        });
    }

    ngOnInit() {
    }

    DeleteItem(i) {
        console.log("Del 00 : ", this.ItemsArray[i]['index'] +  " : " + this.Id);
        this.EmployeeService.DeleteItem('DeleteEmployee', this.ItemsArray[i]['index'] , this.Id).then((data: any) => {
            this.ItemsArray = data , console.log("Del 2 : ", data);
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

}
