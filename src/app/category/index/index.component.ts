import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'category';
    public addButton:string = 'הוסף קטגורייה'


    constructor(public MainService: MainService, settings: SettingsService) {
        console.log("111")
        this.MainService.GetCategories('GetCategories').then((data: any) => {
            console.log("GetCategories : ", data) ,
                this.ItemsArray = data,
                this.ItemsArray1 = data,
                this.host = settings.host,
                this.avatar = settings.avatar,
                console.log(this.ItemsArray[0].logo)
        })
    }

    ngOnInit() {
    }

    DeleteItem(i) {
        console.log("Del 1 : ", this.ItemsArray[i].id);
        this.MainService.DeleteItem('DeleteCategory', this.ItemsArray[i].id).then((data: any) => {
            this.ItemsArray = data , console.log("Del 2 : ", data);
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

}
