import {NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DragulaModule } from 'ng2-dragula/ng2-dragula';


import { OrderRoutes } from './order.routing';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';


import { DataTableComponent } from '../datatable/data-table/data-table.component';
import { TableEditingComponent } from '../datatable/table-editing/table-editing.component';
import { TableFilterComponent } from '../datatable/table-filter/table-filter.component';
import { TablePagingComponent } from '../datatable/table-paging/table-paging.component';
import { TablePinningComponent } from '../datatable/table-pinning/table-pinning.component';
import { TableSelectionComponent } from '../datatable/table-selection/table-selection.component';
import { TableSortingComponent } from '../datatable/table-sorting/table-sorting.component';
import {OrderService} from "./order.service";
import {HttpModule} from "@angular/http";
import {LineaComponent} from "../icons/linea/linea.component";
import {SliComponent} from "../icons/sli/sli.component";
import { EditComponent } from './edit/edit.component';
import { IndexComponent } from './index/index.component';
import { AddComponent } from './add/add.component';
import {ButtonIconsComponent} from "../components/button-icons/button-icons.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FileUploadModule} from "ng2-file-upload";
import {FormsModule, NgControl} from "@angular/forms";




@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(OrderRoutes),
        NgxDatatableModule,
        HttpModule,
        NgbModule,
        FileUploadModule,
        FormsModule
    ],
    declarations: [
        EditComponent,
        IndexComponent,
        AddComponent
    ],
    providers: [OrderService]
})


export class OrderModule {}




